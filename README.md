# gst-kms-playback


Check for connected display

https://stackoverflow.com/questions/71219343/specify-a-display-for-a-sink-in-gstreamer

https://forums.raspberrypi.com/viewtopic.php?t=251397



dependencies

```
git clone https://gitlab.freedesktop.org/mesa/drm
cd drm
sudo apt install meson
mkdir build
meson build
ninja -C build
```


~/src/drm/build/tests/modetest/modetest -M vc4 -c | grep connected

32	0	disconnected	HDMI-A-1       	0x0		0	31
42	0	disconnected	HDMI-A-2       	0x0		0	41
48	47	connected	DSI-1          	154x86		1	47


where 32, 42, 48 are the connector-id for HDMI-1 HDMI-2 and DSI-1 accordingly


gst-launch-1.0 -e -vvv filesrc location=./BigBuckBunny.mp4 ! qtdemux ! queue ! h264parse ! v4l2h264dec ! kmssink connector-id=48
